<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
        <link rel="stylesheet" href="css/styles.40698255d406c0946b85.css">
    </head>
    <body>
        <app-root></app-root>
        <script type="text/javascript" src="js/runtime.a66f828dca56eeb90e02.js"></script>
  <script type="text/javascript" src="js/polyfills.7a0e6866a34e280f48e7.js"></script>
  <script type="text/javascript" src="js/main.abc03c5137b38604f078.js"></script>
    </body>
</html>
