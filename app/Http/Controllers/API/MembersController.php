<?php

namespace App\Http\Controllers\API;

use App\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Get the input params (either post or get).
        $firstName = $request->firstname;
        $surname = $request->surname;
        $email = $request->email;

        // Find the matching data from the DB for the input params. 
        $members = Member::where('firstName', 'like', '%'. $firstName .'%')
                         ->where('surname', 'like', '%'. $surname .'%')
                         ->where('email', 'like', '%'. $email .'%')->get();

        // reutrn the data in json.                          
        return response()->json($members);
        
    }
}
